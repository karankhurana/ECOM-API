module Api::V1
  class BaseController < ApplicationController
    before_action :authenticate
    respond_to? :json

    private

    def authenticate
      @current_user = User.where(token: request.headers['token']).last
      unless @current_user.present?
        render json: { status: 401, message: "Not Authorized" } and return
      end
    end
  end
end
