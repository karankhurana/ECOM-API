module Api::V1
  class CartsController < BaseController
    
    #/api/v1/products/:product_id/cart
    #Cart create API to create cart for the given product_id for the current user.
    #before creation check for the same product if yes , then update the quantity.
    def create
      if params[:quantity].present?
        params[:user_id] = @current_user.id
        cart = check_for_same_product
        if cart.present?
          update_quantity(cart)
          render json: {success: true, message: "Cart product quantity updated"}
        else
          cart = Cart.new(cart_params)
          if cart.save!
            render json: {success: true, message: "Product added Successfully in the cart"}
          else
            render json: {success: false, message: "Failed to add to cart Error: #{cart.errors.full_messages.join(',')}" }
          end
        end
      else
        render json: { sucess: false, message: "Quantity is not present"}
      end
    end

    #get cart for specific user
    #/api/v1/user/:user_id/cart(.:format) 
    def index
      user = User.where(id: params[:user_id])
      if user.present?
        render json:  { success: true, cart: Cart.where(user_id: params[:user_id])}
      else
        render json: { success: false, message: "User Not found" } 
      end
    end

    private

    def cart_params
      params.permit( :quantity, :user_id, :product_id)
    end

    #fetch cart object for params[:product_id]
    def check_for_same_product
      Cart.where(product_id: params[:product_id], user_id: @current_user.id).last
    end

    #update cart product quantity
    def update_quantity(cart)
      cart.quantity += params[:quantity].to_i 
      cart.save!
    end
  end
end
