module Api::V1
  class ProductsController < BaseController

    #/api/v1/products
    def index
      @products = Product.all
      render json: @products
    end
  end
end
