module Api::V1
  class UsersController < BaseController
    
    # GET /users
    def index
      @users = User.all

      render json: { success: true, users: @users }
    end
    
    def login
      render json: { success: true, token: @current_user.token, message: "You are currently Logged-in as #{@current_user.name}" }
    end
  end
end

