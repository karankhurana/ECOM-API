Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :products, only: [:index] do
        resources :carts
      end
      resources :users, only: [:index] do
        resources :carts, only: [:index]
      end

      get 'login' => "users#login"
    end
  end
end
